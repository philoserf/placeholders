# CHANGELOG

## [Unreleased]

## [0.0.0] - yyyy-mm-dd

### Added (for new features)

- note

### Changed (for changes in existing functionality)

- note

### Deprecated (for soon-to-be removed features)

- note

### Removed (for now removed features)

- note

### Fixed (for any bug fixes)

- note

### Security (in case of vulnerabilities)

- note
