# Contributing

Thanks for taking an interest in this open source project. Your support
and involvement is greatly appreciated. The following details what you
need to know in order to contribute.

## Requirements

- Follow these [Basic Programming Styles][basic].
- Follow these [Code Review Styles][review].
- Follow these [Git Styles][git].
- Follow these [Bash Styles][bash].
- Follow these [CSS Styles][css].
- Follow these [Ruby Styles][ruby].

## Contributing Code

0. Read the project README thoroughly before starting.
1. Fork the master branch of the repository.
2. Ensure there are no setup, usage, and/or test issues (again, follow
   the README).
3. Add tests for new functionality (refactoring and documentation
   changes can be excluded).
4. Ensure all tests pass.
5. Push your feature branch and submit a pull request.

## Submitting Issues

0. Submit an issue via the GitHub Issues tab (assuming one does not
   already exist).
1. Clearly describe the issue (including steps to reproduce).
2. Specify your enviroment setup (OS, browser, language, etc. with
   version info).
3. Provide a stack dump (if possible).
4. Explain any additional details that might help diagnose the problem
   quickly.

## Feedback

Expect a response within one to three business days. Changes,
alternatives, and/or improvements might be suggested upon review.

[basic]: https://github.com/bkuhlmann/style_guides/blob/master/programming/basic.md
[review]: https://github.com/bkuhlmann/style_guides/blob/master/programming/code_reviews.md
[git]: https://github.com/bkuhlmann/style_guides/blob/master/programming/git.md
[bash]: https://github.com/bkuhlmann/style_guides/blob/master/programming/languages/bash.md
[css]: https://github.com/bkuhlmann/style_guides/blob/master/programming/languages/css.md
[ruby]: https://github.com/bkuhlmann/style_guides/blob/master/programming/languages/ruby/ruby.md
